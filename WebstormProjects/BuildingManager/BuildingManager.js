/* Created by Dylan on 3/6/14.*/
function On_PluginInit(){
    //** I decided that we should just check for the existence of the INI once...**//
    //** I am also now loading some settings into the hash table.  This should boost performance. **//
    Server.Broadcast("Building Manager Version 2.0");
    if(!Plugin.IniExists("BuildingManager")){
        Server.Broadcast("WARNING: BuildingManager.ini is Missing!");
    }
    else{
        var buildingmanagerINI = Plugin.GetIni("BuildingManager");
        var Sections = buildingmanagerINI.EnumSection("BuildingManagerConfig");
        for(var section in Sections){
            DataStore.Add("BuildingManagerConfig", section, buildingmanagerINI.GetSetting("BuildingManagerConfig", section));
        }
    }
    if(!Plugin.IniExists("BuildingManagerReimbursement")){
        Server.Broadcast("WARNING: BuildingManagerReimbursement.ini is Missing!");
    }
    if(!Plugin.IniExists("BuildingManagerEvents")){
        Plugin.CreateIni("BuildingManagerEvents");
    }
    if(!Plugin.IniExists("BuildingManagerPlayerList")){
        Plugin.CreateIni("BuildingManagerPlayerList");
    }
    if(!Plugin.IniExists("BuildingManagerEmailEvents")){
        Plugin.CreateIni("BuildingManagerEmailEvents");
    }
    if(!Plugin.IniExists("BuildingManagerShares")){
        Plugin.CreateIni("BuildingManagerShares");
    }
    if(!Plugin.IniExists("BuildingManagerEmailList")){
        Plugin.CreateIni("BuildingManagerEmailList");
    }
}
function On_EntityHurt(HurtEvent){
    AlertOnBaseAttack(HurtEvent);
    IsEligible(HurtEvent);
    if(HurtEvent.WeaponName == DataStore.Get("BuildingManagerConfig", "BreakWeapon")){
        if(IsEligible(HurtEvent) == true && (HurtEvent.Entity.OwnerID == HurtEvent.Attacker.PlayerClient.netUser.userID || ShareBreak(HurtEvent))){
            var thisHit = parseInt(' '+new Date().getTime()+' ');
            var managerINI = Plugin.GetIni("BuildingManagerEvents");
            var entityID = managerINI.GetSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "EntityID");
            var timeFirstHit = managerINI.GetSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "ActionTime")
            var difference = parseInt(timeFirstHit) - thisHit;
            var returnItem = LookupEntityName(HurtEvent.Entity.Name);
            var returnQuantity = LookupEntityQuantity(HurtEvent.Entity.Name);

            if(entityID != null){
                if(difference > -3000 && HurtEvent.Entity.Name == managerINI.GetSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "EntityID"))
                {
                    HurtEvent.Entity.Destroy();
                    HurtEvent.Attacker.InventoryNotice(returnQuantity + " x " + returnItem);
                    HurtEvent.Attacker.Inventory.AddItem(returnItem, parseInt(returnQuantity));
                    managerINI.DeleteSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "EntityID");
                    managerINI.DeleteSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "ActionTime");
                    managerINI.Save();
                }

                else if(difference <= -3000){
                    managerINI.DeleteSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "EntityID");
                    managerINI.DeleteSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "ActionTime");
                    managerINI.Save();
                    managerINI.AddSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "EntityID", HurtEvent.Entity.Name);
                    managerINI.AddSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "ActionTime", thisHit);
                    managerINI.Save();
                    HurtEvent.Attacker.Notice("One more hit to break " + LookupEntityPropperName(HurtEvent.Entity.Name));
                }
            }
            else{
                managerINI.AddSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "EntityID", HurtEvent.Entity.Name);
                managerINI.AddSetting(HurtEvent.Attacker.PlayerClient.netUser.userID, "ActionTime", thisHit);
                managerINI.Save();
                HurtEvent.Attacker.Notice("One more hit to break " + LookupEntityPropperName(HurtEvent.Entity.Name));
            }
        }
    }
}

function On_PlayerConnected(netUser){
    var PlayerListINI = Plugin.GetIni("BuildingManagerPlayerList");
    var thisPlayer = PlayerListINI.GetSetting("MasterPlayerList", netUser.PlayerClient.userID.toString());
    if(thisPlayer != null){
        PlayerListINI.SetSetting("MasterPlayerList", netUser.PlayerClient.userID.toString(), netUser.PlayerClient.userName);
        PlayerListINI.Save();
    }
    if(thisPlayer == null){
        PlayerListINI.AddSetting("MasterPlayerList", netUser.PlayerClient.userID.toString(), netUser.PlayerClient.userName);
        PlayerListINI.Save();
    }
}


function ShareBreak(HurtEvent){
    if(Plugin.IniExists("BuildingManagerShares")){
        var sharesINI = Plugin.GetIni("BuildingManagerShares");
        var playerPermission = sharesINI.GetSetting(HurtEvent.Entity.OwnerID, HurtEvent.Attacker.SteamID);
        if(playerPermission == "All"){
            return true
        }
        if(playerPermission == "Base"){
            if(HurtEvent.Entity.IsStructure() || HurtEvent.Entity.Name == "WoodenDoor" || HurtEvent.Entity.Name == "MetalDoor"){
                return true
            }
        }
        if(playerPermission == "Furniture"){
            if(!HurtEvent.Entity.IsStructure() && HurtEvent.Entity.Name != "WoodenDoor" && HurtEvent.Entity.Name != "MetalDoor"){
                return true;
            }
        }
    }
}

function IsEligible(HurtEvent){
    try{
        var Eligible = HurtEvent.Entity.Object._master.ComponentCarryingWeight(HurtEvent.Entity.Object);
        return !Eligible;
    }
    catch(err){
        return true;
    }
}

function AlertOnBaseAttack(HurtEvent){
    var player = Magma.Player.FindBySteamID(HurtEvent.Entity.OwnerID.toString());
    if(HurtEvent.DamageEvent.damageTypes.value__ == 8 && (HurtEvent.Entity.OwnerID.toString() != HurtEvent.Attacker.SteamID)){
        var emailEvents = Plugin.GetIni("BuildingManagerEmailEvents");
        var eventSection = emailEvents.EnumSection("BaseAlertEvents");
        var eventAlreadyOccurred = false;
        for(var event in eventSection){
            var eventOwner = emailEvents.GetSetting("BaseAlertEvents", event);
            if(eventOwner == HurtEvent.Entity.OwnerID.toString()){
                eventAlreadyOccurred = true;
            }
        }
        if(eventAlreadyOccurred == false){
            emailEvents.AddSetting("BaseAlertEvents", HurtEvent.Entity.OwnerID.toString(), HurtEvent.Entity.OwnerID.toString());
            emailEvents.Save();
        }
    }
}

/* This function will look for the quotes that surround a players name.  It will work on any size name, provided the
** player name is at the end of the command, and the players name start at the second position.*/
function GetPlayerName(args){
    if(!args[1].StartsWith("\"")){
        return args[1].toString();
    }
    else{
        var firstArg = args[1].replace("\"", '');
        var argReturn = firstArg.toString();
        for(var i = 2; i < args.Length; i++){
            var newArg = args[i].replace("\"", '');
            argReturn = argReturn + " " + newArg.toString();
        }
        return argReturn.toString();
    }
}

function On_Command(Player, cmd, args){
    var buildingmanagerINI = Plugin.GetIni("BuildingManager");
    var isEmailEnabled = DataStore.Get("BuildingManagerConfig", "EnabledEmail")
    if(cmd == "baseshare" && args.Length == 0){
        Player.Message("Please type /baseshare help for a list of commands.");
    }
    if(cmd == "baseshare"){
        if(args[0] == "show"){
            var sharesINI = Plugin.GetIni("BuildingManagerShares");
            Player.Message("Players You Share With: ");
            var playerList = sharesINI.EnumSection(Player.SteamID);
            if(playerList.Length < 1){
                Player.Message("Your share list is empty.");
            }
            for(var playerID in playerList){
                var playerPermission = sharesINI.GetSetting(Player.SteamID, playerID);
                if(playerID != "Share"){
                    Player.Message(LookupPlayerName(playerID) + " has " + playerPermission + " access.");
                }
            }
            var allPlayersINI = Plugin.GetIni("BuildingManagerPlayerList");
            var allPlayers = allPlayersINI.EnumSection("MasterPlayerList");

            Player.Message("-------------------");
            Player.Message("Players Sharing With You: ");
            for(var player in allPlayers){
                var playerShare = sharesINI.EnumSection(player);

                for(var singlePlayer in playerShare){
                    if(singlePlayer == Player.SteamID){
                        Player.Message(LookupPlayerName(player) + " has given you " + sharesINI.GetSetting(player, singlePlayer) + " access.");
                    }
                }

            }
        }
        else if(args[0] == "all"){
            var sharesINI = Plugin.GetIni("BuildingManagerShares");
            var playerID = Magma.Player.FindByName(GetPlayerName(args));
            if(playerID == null){
                Player.Notice("Could not find " + GetPlayerName(args));
                return;
            }
            var playerPermission = sharesINI.GetSetting(Player.SteamID, playerID.SteamID);
            if(playerPermission == null){
                sharesINI.AddSetting(Player.SteamID, playerID.SteamID, "All");
            }
            if(playerPermission != "All"){
                sharesINI.SetSetting(Player.SteamID, playerID.SteamID, "All");
            }
            if(playerPermission == "All"){
                Player.Message(playerID.Name + " already has base access!");
                return;
            }
            Player.Notice("You have shared your everything with " + GetPlayerName(args));
            playerID.Notice(Player.Name + " has shared all their things with you!");
            sharesINI.Save();
        }
        else if(args[0] == "base"){
            var sharesINI = Plugin.GetIni("BuildingManagerShares");
            var playerID = Magma.Player.FindByName(GetPlayerName(args));
            if(playerID == null){
                Player.Notice("Could not find " + GetPlayerName(args));
                return;
            }
            var playerPermission = sharesINI.GetSetting(Player.SteamID, playerID.SteamID);
            if(playerPermission == null){
                sharesINI.AddSetting(Player.SteamID, playerID.SteamID, "Base");
            }
            if(playerPermission != "Base"){
                sharesINI.SetSetting(Player.SteamID, playerID.SteamID, "Base");
            }
            if(playerPermission == "Base"){
                Player.Message(playerID.Name + " already has base access!");
                return;
            }
            Player.Notice("You have shared your base with " + GetPlayerName(args));
            playerID.Notice(Player.Name + " has shared their base with you!");
            sharesINI.Save();
        }
        else if(args[0] == "furniture"){
            var targetPlayer = args[1];
            var sharesINI = Plugin.GetIni("BuildingManagerShares");
            var playerID = Magma.Player.FindByName(GetPlayerName(args));
            if(playerID == null){
                Player.Notice("Could not find " + GetPlayerName(args));
                return;
            }
            var playerPermission = sharesINI.GetSetting(Player.SteamID, playerID.SteamID);
            if(playerPermission == null){
                sharesINI.AddSetting(Player.SteamID, playerID.SteamID, "Furniture");
            }
            if(playerPermission != "Furniture"){
                sharesINI.SetSetting(Player.SteamID, playerID.SteamID, "Furniture");
            }
            if(playerPermission == "Furniture"){
                Player.Message(playerID.Name + " already has base access!");
                return;
            }
            Player.Notice("You have shared your furniture with " + GetPlayerName(args));
            playerID.Notice(Player.Name + " has shared their furniture with you!");
            sharesINI.Save();
        }
        else if(args[0] == "help"){
            Player.Message("BaseManager Mod v2.0");
            Player.Message("For All Your Base Building Needs!!");
            Player.Message("/baseshare all PlayerName - Shares all objects placed by you.");
            Player.Message("/baseshare base PlayerName - Shares only structure objects placed by you.");
            Player.Message("/baseshare furniture PlayerName - Shares only non-structure objects placed by you.");
            Player.Message("/baseshare show - Shows what access you have given to who.");
            Player.Message("/baseshare remove PlayerName - Removes a players permissions to objects placed by you.");
            if(isEmailEnabled == "Enabled"){
                Player.Message("/baseshare register EmailAddress - Registers your email address for event alerts");
                Player.Message("/baseshare sharealerts on\off - choose on or off. this will set whether or not to send alerts to the people you share your base with");
            }
        }
        else if (args[0] == "remove"){
            var targetPlayer = args[1];
            var sharesINI = Plugin.GetIni("BuildingManagerShares");
            var playerID = Magma.Player.FindByName(GetPlayerName(args));
            var playerPermission = sharesINI.GetSetting(Player.SteamID, playerID.SteamID);
            if(playerPermission == null){
                Player.Notice("That player doesn't have access.");
                return;
            }
            else{
                var targetPlayer = args[1];
                sharesINI.DeleteSetting(Player.SteamID, playerID.SteamID);
                sharesINI.Save();
                Player.Notice("You have revoked " + playerID.Name + "'s " + playerPermission + " access.");
                playerID.Notice(Player.Name + " revoked your access to his base and furniture items.");
            }
        }
        else if(args[0] == "register" && isEmailEnabled == "Enabled"){
            if(args[1].Contains("@")){
                RegisterPlayerEmail(Player, args[1]);
            }
            else{
                Player.Message("Please enter a valid Email Address.");
            }
        }
        else if(args[0] == "sharealerts" && isEmailEnabled == "Enabled"){
            var sharesINI = Plugin.GetIni("BuildingManagerShares");
            if(sharesINI.GetSetting(Player.SteamID, "Share")){
                sharesINI.AddSetting(Player.SteamID, "Share");
                sharesINI.Save();
            }
            if(args[1] == "on"){
                sharesINI.AddSetting(Player.SteamID, "Share", "on");
                sharesINI.Save();
            }
            else if (args[1] == "off"){
                sharesINI.AddSetting(Player.SteamID, "Share", "off");
                sharesINI.Save();
            }
        }
        else if (args[0] == "startEmail" && Player.Admin == true){
            if(isEmailEnabled == "Enabled"){
                Player.Message("Email Timer Started.");
                DataStore.Add("EmailStats", "StartDate", System.DateTime.Now);
                DataStore.Add("EmailStats", "LastEmail", "-------");
                DataStore.Add("EmailStats", "OpsState", "Running");
                DataStore.Add("EmailStats", "EmailSent", 0);
                Plugin.CreateTimer("SendBaseAlerts", 30000).Start();
            }
            else{
                Player.Message("Email is not enabled.");
            }
        }
        else if (args[0] == "stopEmail" && Player.Admin == true){
            if(isEmailEnabled == "Enabled"){
                Player.Message("Email Timer Stopped.");
                DataStore.Remove("EmailStats", "StartDate", "--------");
                DataStore.Add("EmailStats", "OpsState", "Stopped");
                Plugin.KillTimer("SendBaseAlerts");
            }
            else{
                Player.Message("Email is not enabled.");
            }
        }
        else if (args[0] == "getEmailStats"){
            if(isEmailEnabled == "Enabled"){
                Player.Message("---Email Stats---");
                Player.Message("Start Date: " + DataStore.Get("EmailStats", "StartDate"));
                Player.Message("Last Email: " + DataStore.Get("EmailStats", "LastEmail"));
                Player.Message("Status: " + DataStore.Get("EmailStats", "OpsState"));
                Player.Message("Emails Sent: " + DataStore.Get("EmailStats", "EmailSent"));
            }
            else{
                Player.Message("Email is not enabled.");
            }
        }
        else if(args[0] == "disableEmail" && Player.Admin == true){
            buildingmanagerINI.SetSetting("BuildingManagerConfig", "EnabledEmail", "Disabled");
            DataStore.Add("BuildingManagerConfig", "EnabledEmail", "Disabled");
            buildingmanagerINI.Save();
            Plugin.KillTimer("SendBaseAlerts");
            Player.Message("Email alerts have been disabled.");
        }
        else if(args[0] == "enableEmail" && Player.Admin == true){
            buildingmanagerINI.SetSetting("BuildingManagerConfig", "EnabledEmail", "Enabled");
            DataStore.Add("BuildingManagerConfig", "EnabledEmail", "Enabled");
            buildingmanagerINI.Save();
            Player.Message("Email alerts have been enabled.");
        }
        else{
            Player.Message("Incorrect Syntax, please type /baseshare help for a list of commands");
        }
    }
}

function RegisterPlayerEmail(Player, emailAddress){
    var emailINI = Plugin.GetIni("BuildingManagerEmailList");

    if(emailINI.GetSetting("EmailList", Player.SteamID) == null){
        emailINI.AddSetting("EmailList", Player.SteamID, emailAddress);
        emailINI.Save();
    }
    else{
        emailINI.SetSetting("EmailList", Player.SteamID, emailAddress);
        emailINI.Save();
    }

    Player.Notice("Your email has been registered!");
}

//I need to abstract the POST URL to an external config file.
function SendBaseAlertsCallback(){
    var emailSent = DataStore.Get("EmailStats", "EmailSent");
    var emailEvents = Plugin.GetIni("BuildingManagerEmailEvents");
    var emailList = Plugin.GetIni("BuildingManagerEmailList");
    var buildingShares = Plugin.GetIni("BuildingManagerShares");
    var eventSection = emailEvents.EnumSection("BaseAlertEvents");
    for(var event in eventSection){
        var eventOwner = emailEvents.GetSetting("BaseAlertEvents", event);
        var emailAddress = emailList.GetSetting("EmailList", eventOwner);
        if(buildingShares.GetSetting(eventOwner, "Share") == "on"){
            var shareList = buildingShares.EnumSection(eventOwner);
            for(var share in shareList){
                DataStore.Add("EmailStats", "EmailSent", (parseInt(emailSent) + 1));
                DataStore.Add("EmailStats", "LastEmail", System.DateTime.Now);
                Web.POST("http://rustplugin.webuda.com/", "user=" + emailList.GetSetting("EmailList", share.toString()) + "&message=A base shared with you has been attacked!");
            }
        }
        DataStore.Add("EmailStats", "EmailSent", (parseInt(emailSent) + 1));
        DataStore.Add("EmailStats", "LastEmail", System.DateTime.Now);
        Web.POST(DataStore.Get("BuildingManagerConfig", "EmailPostURL"), "user=" + emailAddress + "&message=You have been attacked!");
        emailEvents.DeleteSetting("BaseAlertEvents", event);
        emailEvents.Save();
    }

}

function LookupPlayerName(SteamID){
    var PlayerListINI = Plugin.GetIni("BuildingManagerPlayerList");
    return PlayerListINI.GetSetting("MasterPlayerList", SteamID);
}

function LookupEntityName(Entity){
    var reimbursementINI = Plugin.GetIni("BuildingManagerReimbursement");
    var ItemName = reimbursementINI.GetSetting(Entity, "Item");
    return ItemName;
}
function LookupEntityQuantity(Entity){
    var reimbursementINI = Plugin.GetIni("BuildingManagerReimbursement");
    var ItemQuantity = reimbursementINI.GetSetting(Entity, "Quantity");
    return parseInt(ItemQuantity);
}
function LookupEntityPropperName(EntityName){
    var objectNameDatabase = [
        ["Metal Ceiling", "MetalCeiling"],["Metal Doorway", "MetalDoorFrame"],["Metal Foundation", "MetalFoundation"],["Metal Pillar", "MetalPillar"],["Metal Ramp", "MetalRamp"],["Metal Stairs", "MetalStairs"],["Metal Wall", "MetalWall"],["Metal Window", "MetalWindowFrame"],
        ["Wood Ceiling", "WoodCeiling"],["Wood Doorway", "WoodDoorway"],["Wood Foundation", "WoodFoundation"],["Wood Pillar", "WoodPillar"],["Wood Ramp", "WoodRamp"],["Wood Stairs", "WoodStairs"],["Wood Wall", "WoodWall"],["Wood Window", "WoodWindowFrame"],
        ["Camp Fire", "Campfire"],["Furnace", "Furnace"],["Large Spike Wall", "LargeWoodSpikeWall"],["Large Wood Storage", "WoodBoxLarge"],["Metal Door", "MetalDoor"],["Repair Bench", "RepairBench"],["Spike Wall", "WoodSpikeWall"],["Wood Barricade", "Barricade_Fence_Deployable"],
        ["Wood Gate", "WoodGate"],["Wood Gateway", "WoodGateway"],["Wood Shelter", "Wood_Shelter"],["Wood Storage Box", "WoodBox"],["Wood Door", "WoodenDoor"],["Workbench", "Workbench"]
    ];
    for(var i=0; i < objectNameDatabase.length; i++){
       if(EntityName == objectNameDatabase[i][1]){
           return objectNameDatabase[i][0];
       }
    }
    return null;
}

